const axios = require('axios');
var _ = require('lodash');

const getItemsData = (itemSearch, onlyItem) => {
    const price = itemSearch.price.toString().split('.');
    let dataItem = {};
    dataItem = onlyItem && ({
        sold_quantity: itemSearch.sold_quantity,
        description: itemSearch.description
    })
    const dataCity = !onlyItem ? {
        city_name: itemSearch.address.city_name
    }: {}
    return {
        id: itemSearch.id,
        title: itemSearch.title,
        price: {
            currency: itemSearch.currency_id,
            amount: itemSearch.price,
            decimals: price[1] ? price[1] : 0
        },
        picture: itemSearch.thumbnail,
        condition: itemSearch.condition,
        free_shipping: itemSearch.shipping.free_shipping,
        ...dataCity,
        ...dataItem
    };
};

const intanceHttp = (url, params) => {
    return axios.create({
        baseURL: url,
        params
    })
}


const getCategories = (filters, available_filters) => {
    const categoriesFilters = filters.find(cat => cat.id === 'category');
    const availableFilters = available_filters.find(cat => cat.id === 'category');
    let categories = [];
    if (!_.isEmpty(filters) && categoriesFilters) {
        categories = categoriesFilters.values.map(item => item.name)
        const path = _.find(categoriesFilters.values, function(res) { return res.path_from_root.length > 0 });
        return {
            categories,
            path: path.path_from_root || []
        }
    }else if( !_.isEmpty(available_filters) && availableFilters){
        const data = _.orderBy(availableFilters.values, 'results', ['desc']);
        categories = availableFilters.values.map(item => item.name);
        const path = [{
            name: data[0].name,
            id: data[0].id,
            results: data[0].results
        }]
        return {
            categories,
            path
        }
    }
    return {
        categories: [],
        path: []
    };
}

module.exports = {
    getItemsData,
    intanceHttp,
    getCategories
}


