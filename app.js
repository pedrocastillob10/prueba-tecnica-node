require('dotenv').config();
var express = require('express');
var path = require('path');

var indexRouter = require('./routes/index');

var app = express();
const port = process.env.PORT;

console.log(port)

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.listen( port, () => {
    console.log('Servidor corriendo en puerto', port );
});


module.exports = app;
