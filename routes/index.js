var express = require('express');
var router = express.Router();

var itemsRoute = require('./items');

router.use('/items', itemsRoute);

module.exports = router;
