const router = require('express').Router();
const controller = require('../controllers/items');


router.use(function timeLog(req, res, next) {
    next();
});
router.get('/', controller.getItems);
router.get('/:id', controller.getItem)

module.exports = router;
