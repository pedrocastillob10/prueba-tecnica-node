const responseItems = require('../models/items');
const utils = require('../utils/utils');
const controller = {};

const response = new responseItems();

controller.getItems = async (req, res) => {
    try{
        const { q } = req.query;
        const params = {
            'limit': 4,
            'q': q
        };
        const intance = utils.intanceHttp('https://api.mercadolibre.com/sites/MLA/search', params)
        const dataApi =  await intance.get();

        let items = [];
        dataApi.data.results.map( itemSearch => {
            const itemData = utils.getItemsData(itemSearch, false);
            items.push(itemData);
        });
        const filters = dataApi.data.filters;
        const available_filters = dataApi.data.available_filters;
        const {categories, path} = utils.getCategories(filters, available_filters);
        const data = response.setFormatItems(items, categories, path);
        res.status(200).json({ok:true, data});
    } catch (e){
        res.status(500).json({ok:false});
    }
};

controller.getItem = async (req, res) => {
    const { id } = req.params;
    try{
        const intance = utils.intanceHttp(`https://api.mercadolibre.com/items/${id}`, null);
        const dataApi =  await intance.get();

        const intanceDescription = utils.intanceHttp(`https://api.mercadolibre.com/items/${id}/description`, null)
        const dataDescription =  await intanceDescription.get();
        dataApi.data.description = dataDescription.data.plain_text;
        const itemData = utils.getItemsData(dataApi.data, true);
        const data = response.setFormatItem(itemData);
        res.status(200).json({ok:true, data});
    } catch (e){
        res.status(500).json({ok:false});
    }
}

module.exports = controller;
