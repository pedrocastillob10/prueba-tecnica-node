
const autor = {
    name: 'Pedro',
    lastname: 'Castillo'
};
class responseItems {
    setFormatItems = (items, categories, path) => ({
        autor,
        items,
        categories,
        path
    })

    setFormatItem = (item) => ({
        autor,
        item
    });
}

module.exports = responseItems;
